$(function() {

// Фиксированное меню при прокрутке страницы вниз
if ($(window).width() >= '768'){
$(window).scroll(function(){
        var sticky = $('.navbar'),
            scroll = $(window).scrollTop();
        if (scroll > 200) {
            sticky.addClass('navbar-fixed');
        } else {
            sticky.removeClass('navbar-fixed');
        };
    });
}



// Фиксированные услуги внизу странцы

$(window).scroll(function(){
        var sticky = $('.selected-services'),
            scroll = $(window).scrollTop() + $(window).height();
            windowHeight = $(document).height();
        if (scroll > windowHeight - 250) {
            sticky.removeClass('fixed');
        } else {
            sticky.addClass('fixed');
        };
    });


// Слайдер сравнения двух изображений
$(window).load(function() {
  $(".image-comparison").twentytwenty();
});

// меню у авторизованного пользователя
$('.header-user').click(function() {
  $('.header-user__menu').fadeToggle();
})


// BEGIN of script for .banner-slider
var bannerSlider =  $('.banner-slider').slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    // autoplay: true,
    // autoplaySpeed: 4000,
    fade: true,
    pauseOnHover: false,
    arrows: false,
    dots: true
});

// BEGIN of script for .shares-slider
var sharesSlider =  $('.shares-slider').slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    // autoplay: true,
    // autoplaySpeed: 4000,
    fade: true,
    pauseOnHover: false,
    arrows: false,
    dots: true
});
$('.shares-slider__prev').click(function(){
  $(sharesSlider).slick("slickPrev");
});
$('.shares-slider__next').click(function(){
  $(sharesSlider).slick("slickNext");
 });



if ($(window).width() >= '768'){
var windowWidth = $( window ).width();
var containerWidth = $( ".container" ).width();
$('.shares-slider .slick-dots').css("left", (windowWidth-containerWidth )/2 + 70);
}




$('.documents-slider').slick({
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    // autoplay: true,
    // autoplaySpeed: 4000,
    pauseOnHover: false,
    arrows: true,
    dots: false,
    responsive: [
    {
      breakpoint: 1200,
      settings: {
        slidesToShow: 2,
      }
    },
    {
      breakpoint: 767,
      settings: {
        slidesToShow: 1,
      }
    },
  ]
});

$('.work-examples__slider').slick({
    infinite: true,
    slidesToShow: 2,
    slidesToScroll: 1,
    draggable: false,
    // autoplay: true,
    // autoplaySpeed: 4000,
    pauseOnHover: false,
    arrows: true,
    dots: false,
     responsive: [
    {
      breakpoint: 767,
      settings: {
        slidesToShow: 1,
      }
    },
  ]
});

$('.full-page__slider').slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    // autoplay: true,
    // autoplaySpeed: 4000,
    pauseOnHover: false,
    arrows: true,
    dots: false
});

var gallerySlider = $('.gallery-slider').slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    // autoplay: true,
    // autoplaySpeed: 4000,
    pauseOnHover: false,
    arrows: true,
    dots: false
});


var tabsNav = $('.tabs-nav').slick({
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 1,
    // autoplay: true,
    // autoplaySpeed: 4000,
    pauseOnHover: false,
    arrows: false,
    dots: false,
    variableWidth: true
});
$('.tabs-nav__prev').click(function(){
  $(tabsNav).slick("slickPrev");
});
$('.tabs-nav__next').click(function(){
  $(tabsNav).slick("slickNext");
});

// Стилизация селектов
$('select').styler();



// УСЛУГИ
// Добавить услугу
$('.btn-add a').click(function() {
  $(this).toggleClass('active');
})
// Поменять текст услуги при наведении

$('.our-services__item').hover(function() {
var currentInfo = $(this).children('.our-services__text').clone();
$('.our-services__description').empty().append(currentInfo);
});


// Добавить услугу в модальном окне
$('.modal-services__item').click(function() {
  $(this).toggleClass('active');
})





// Табы
$('.tabs-nav a').click(function() {
    gallerySlider.slick('refresh');
    var _id = $(this).attr('href');
    var _targetElement = $(this).parents('.tabs-wrapper').find('#' + _id);
    $('.tabs-nav a').removeClass('active');
    $(this).addClass('active');
    $('.tabs-item').removeClass('active');
    _targetElement.addClass('active');
    return false;
    

});


$('.tabs-mobile__btn').click(function() {
    gallerySlider.slick('refresh');
    var _targetElementParent = $(this).parents('.tabs-item');
    _targetElementParent.toggleClass('active-m');

    _targetElementParent.find('.tabs-item__body').slideToggle(600);
    $('.tabs-mobile__btn').parent('.tabs-item').removeClass('active');
    return false;


});





$('.partners-slider').slick({
    infinite: true,
    slidesToShow: 4,
    slidesToScroll: 1,
    // autoplay: true,
    // autoplaySpeed: 4000,
    pauseOnHover: false,
    responsive: [
    {
      breakpoint: 1200,
      settings: {
        slidesToShow: 3,
      }
    },
    {
      breakpoint: 991,
      settings: {
        slidesToShow: 2,
      }
    },
  ]
});


// BEGIN of script for header submenu
    $(".navbar-toggle").on("click", function () {
        $(this).toggleClass("active");
    });

// Поиск в шапке
$('.btn-search').click(function() {
  $(this).toggleClass('active');
  $('.header-search').fadeToggle();
})


// Выпадающее меню
if($(window).width() >= 768){

	$('.dropdown').hover(function() {
		$(this).children('a').stop(true,true).toggleClass('active');
		$(this).children(".dropdown-menu").stop(true,true).fadeToggle();
	})

	$('.dropdown-menu > li').hover(function() {
	    $(this).children('ul').toggleClass('active');
	})


}


if($(window).width() < 768){

	$('.navbar-toggle').click(function() {
		$('.navbar-collapse').toggleClass('in');
		$('.navbar-nav').toggleClass('active');
	})

  $('.mobile-toggler').click(function() {
    $(this).toggleClass('active');
    $(this).parent('a').next(".dropdown-menu").stop(true,true).fadeToggle();
  })

}



// Аккордеон
$('.faq-header').click(function(){

if(!($(this).next().hasClass('active'))){
  $('.faq-body').slideUp().removeClass('active');
  $(this).next().slideDown().addClass('active');
}else{
  $('.faq-body').slideUp().removeClass('active');
};

if(!($(this).parent('.faq-item').hasClass('active'))){
  $('.faq-item').removeClass("active");
  $(this).parent('.faq-item').addClass("active");
}else{
  $(this).parent('.faq-item').removeClass("active");

};

});



// FansyBox 2
 $('.fancybox').fancybox({
  beforeShow : function(){
   this.title =  this.title + " - " + $(this.element).data("caption");
   $('select').styler('refresh');
  }
 });



// Показать\скрыть пароль
// сори за костыль)

 $('.pass').click(function() { 
    var temp = document.getElementById("pass");
    if (temp.type === "password") { 
        temp.type = "text"; 
    } 
    else { 
        temp.type = "password"; 
    } 
}) 


 $('.pass-repeat').click(function() { 
    var temp = document.getElementById("pass-repeat");
    if (temp.type === "password") { 
        temp.type = "text"; 
    } 
    else { 
        temp.type = "password"; 
    } 
}) 

$('.pass-input').click(function() { 
    var temp = document.getElementById("pass-input");
    if (temp.type === "password") { 
        temp.type = "text"; 
    } 
    else { 
        temp.type = "password"; 
    } 
}) 


// Скрипт скролла по ID
  $("a.scroll").on("click", function(event) {
      //отменяем стандартную обработку нажатия по ссылке
      event.preventDefault();

      //забираем идентификатор бока с атрибута href
      var id = $(this).attr('href'),

          //узнаем высоту от начала страницы до блока на который ссылается якорь
          top = $(id).offset().top;

      //анимируем переход на расстояние - top за 1500 мс
      $('body,html').animate({ scrollTop: top }, 1000);
  });


// Закрыть модальное окно
$('.modal-box__close').click(function() {
  $(this).parent(".modal-box").fadeOut();
})


//  Скролл вверх
$('.back-top').on('click', function(e) {
    e.preventDefault();
    $('html,body').animate({
        scrollTop: 0
    }, 700);
});




//Begin of GoogleMaps
  var myCenter=new google.maps.LatLng(56.041942, 92.843931);
  var marker;
  function initialize()
  {
    var mapProp = {
    center:myCenter,
    zoom:16,
    disableDefaultUI:true,
    zoomControl: true,
    scrollwheel: false,
    styles: [{"featureType":"water","elementType":"geometry","stylers":[{"color":"#e9e9e9"},{"lightness":17}]},{"featureType":"landscape","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":20}]},{"featureType":"road.highway","elementType":"geometry.fill","stylers":[{"color":"#ffffff"},{"lightness":17}]},{"featureType":"road.highway","elementType":"geometry.stroke","stylers":[{"color":"#ffffff"},{"lightness":29},{"weight":0.2}]},{"featureType":"road.arterial","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":18}]},{"featureType":"road.local","elementType":"geometry","stylers":[{"color":"#ffffff"},{"lightness":16}]},{"featureType":"poi","elementType":"geometry","stylers":[{"color":"#f5f5f5"},{"lightness":21}]},{"featureType":"poi.park","elementType":"geometry","stylers":[{"color":"#dedede"},{"lightness":21}]},{"elementType":"labels.text.stroke","stylers":[{"visibility":"on"},{"color":"#ffffff"},{"lightness":16}]},{"elementType":"labels.text.fill","stylers":[{"saturation":36},{"color":"#333333"},{"lightness":40}]},{"elementType":"labels.icon","stylers":[{"visibility":"off"}]},{"featureType":"transit","elementType":"geometry","stylers":[{"color":"#f2f2f2"},{"lightness":19}]},{"featureType":"administrative","elementType":"geometry.fill","stylers":[{"color":"#000"},{"lightness":80}]},{"featureType":"administrative","elementType":"geometry.stroke","stylers":[{"color":"#000"},{"lightness":70},{"weight":1.2}]}],
    zoomControlOptions: {
        position: google.maps.ControlPosition.RIGHT_CENTER
    },
    mapTypeId:google.maps.MapTypeId.ROADMAP
    };
      var map=new google.maps.Map(document.getElementById("googleMap1"),mapProp);
      var marker=new google.maps.Marker({
      position:myCenter,
      icon: "img/marker.png"
      // icon: "../img/marker.png" на  хосте может так рабоать     
      });
    marker.setMap(map);
    var content = document.createElement('div');
    content.innerHTML = '<strong class="maps-caption">Красноярский завод строп</strong>';
    var infowindow = new google.maps.InfoWindow({
     content: content
    });
      google.maps.event.addListener(marker,'click',function() {
        infowindow.open(map, marker);
        map.setCenter(marker.getPosition());
      });
      }
      google.maps.event.addDomListener(window, 'load', initialize);
//End of GoogleMaps

})